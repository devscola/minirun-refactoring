# Checkout.java

## Requisitos

- `Java 10`

## Preparación

### Ejecutar tests

Para lanzar los test, ejecuta: `./mvnw test`

### Ejecución

Podemos ejecutar el programa de command line que hemos preparado para hacer pruebas manuales del Checkout.
Puedes introducir una cadena con los articulos y te devuelve el precio final.

- `./mvnw package`
- `java -jar target/minirun-refactoring-java-0.0.1.jar`
