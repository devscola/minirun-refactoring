package com.devscola.refactoring;

import java.util.List;
import java.util.Arrays;
import java.util.Scanner;

public class RefactoringApplication {

    public static void main(String[] args) {
        System.out.println("Put items to buy: ");
        Checkout checkout = new Checkout(new PricingRules());

        Scanner scanner = new Scanner(System.in);
        List<String> items = Arrays.asList(scanner.nextLine().split(""));
        items.forEach(item -> { checkout.scan(item); });
        System.out.println(String.format("Total price: %s", checkout.total()));
        scanner.close();
    }

}
