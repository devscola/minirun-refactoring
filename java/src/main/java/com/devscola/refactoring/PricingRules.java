package com.devscola.refactoring;

import java.util.HashMap;
import java.util.Map;

public class PricingRules {

    private Map<String, Integer> table;

    public PricingRules() {
        this.table = new HashMap<>();
        this.table.put("A", 50);
        this.table.put("B", 30);
        this.table.put("C", 20);
        this.table.put("D", 15);
    }

    public Map<String, Integer> getTable() {
        return table;
    }
}
