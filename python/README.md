# Checkout.rb

## Requisitos

Para poder trabajar sobre este ejercicio te basta con tener instalado `docker`. Si prefieres la instalación en local, necesitarás `python3` y tener instalado `pipenv`.

## Como usar con docker

En la carpeta `docker` hay varios scripts, cada uno sirve para una fase distinta del ejercicio.

### docker/console

Sirve para entrar a una consola interactiva de iPython para poder ejecutar código:

```terminal
$ docker/console

To quit, run:
    exit

Python 3.7.3 (default, Mar 27 2019, 23:48:15)
Type 'copyright', 'credits' or 'license' for more information
IPython 7.4.0 -- An enhanced Interactive Python. Type '?' for help.

In [1]:
```

### docker/run

Sirve para ejecutar un programa de command line que hemos preparado para hacer pruebas manuales del Checkout. Puedes introducir una cadena con los articulos y te devuelve el precio final:

```terminal
$ docker/run

Puts items to buy: ABC
Total price: 100
```

### docker/test

Sirve para lanzar todos los test de `mamba`. Pero... ¿donde están los tests?

```terminal
$ docker/test

Checkout
  ✓ it works

1 examples ran in 0.0086 seconds
```

## Como usar en el entorno local

### Preparación

Ejecuta en tu terminal:

```terminal
$ export PYTHONPATH=./src
$ pipenv install --dev --three
```

Esto instalará `mamba`, para que puedas lanzar los tests.

### Lanzar los test

```terminal
$ pipenv run mamba -f documentation

Checkout
  ✓ it works

1 examples ran in 0.0056 seconds
```

### Ejecutar el programa de command line

```terminal
$ pipenv run python src/cli.py
Puts items to buy: ABC
Total price: 100
```
