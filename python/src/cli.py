from checkout import Checkout
from pricing_rules import PricingRules

goods = input("Puts items to buy: ")

checkout = Checkout(PricingRules())

for item in goods:
    checkout.scan(item)

print("Total price: {}".format(checkout.total()))
