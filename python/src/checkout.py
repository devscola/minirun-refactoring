class Checkout:

    def __init__(self, rules):
        self.items = []
        self.rules = rules
        self.result = 0
        self.items2 = []

    def scan(self, item):
        self.items.append(item)

    def total(self):
        for e in self.items:
            self.items2.append(e)
            # Get individual price
            self.result += self.rules.TABLE[e]
            i = 0
            for ee in self.items2:
                if ee == e: i += 1
            self.result -= 20 if e == 'A' and i % 3 == 0 else 15 if e == 'B' and i % 2 == 0 else 0

        return self.result
